(asdf:defsystem #:cl-collection-manager-redux
  :version "0.0.1"
  :description "A package to manage collections."
  :serial t
  :license "GPLv3"
  :components
  ((:file "package")
   (:file "collection-manager-redux"))
  :depends-on (:cl-slug))
