(in-package :cl-collection-manager-redux)

(defvar *db* nil
  "Database for the collection instances.")

(defvar *types* nil
  "List of collection types.")

(defvar *traits* nil
  "List of collection instance traits.")

(defvar *values* nil
  "List of collection instance values.")

(defun collect-trait (trait)
    "Adds traits/categories to the *traits* list unless they are already
present."
  (unless (member trait *traits* :test #'equalp)
    (push trait *traits*)))

(defun collect-value (value)
  "Adds values to the *values* list unless they are already
present."
  (unless (member value *values* :test #'equalp)
    (push value *values*)))

(defun collect-traits-and-values (trait-value-list)
  (if (null trait-value-list) nil
      (progn (collect-trait (first trait-value-list))
	     (collect-value (second trait-value-list))
	     (collect-traits-and-values (cddr trait-value-list)))))

(defun collect-type (type)
  "Add TYPE to the *TYPES* list unless it is already present."
  (unless (member type *types* :test #'equalp)
    (push type *types*)))

(defun collect-types (database)
  "Populate *TYPES* list from using the database."
  (mapcar #'(lambda (instance)
	      (collect-type (second instance)))
	  database))

(defun list-database-entries (kind)
  "List entries for database KIND.  Known database kinds are 'type',
'trait' and 'value'."
  (ecase kind
    ((type types) *types*)
    ((trait traits) *traits*)
    ((value values) *values*)))

(defun as-keyword (symbol)
  "Translate SYMBOL to a keyword."
  (intern (string symbol) :keyword))

(defun create-instance (list)
  "Turn a list of elements into a plist turning each odd-numbered element
into a keyword and each even-numbered element into the corresponding
value."
  (cond ((null list) nil)
	((and (first list)
	      (rest list))
	 (cons (as-keyword (first list))
	       (cons (second list)
		     (create-instance (cddr list)))))))

(defun add-instance (type trait-value-list)
  "Add a collection instance to the database."
  (let ((instance (create-instance
		   (append (list 'type type) trait-value-list))))
    (collect-type type)
    (collect-traits-and-values trait-value-list)
    (unless (member instance *db* :test #'equalp)
      (push instance *db*))))

(defun find-matching (identifier)
  "Return entries that match IDENTIFIER."
  (let ((database *db*))
    (labels
	((remove-non-matching (db id)
	   (cond ((null db) nil)
		 ((member id (car db) :test #'equalp)
		  (cons (car db)
			(remove-non-matching (cdr db) id)))
		 (t (remove-non-matching (cdr db) id)))))      
      (remove-non-matching database identifier))))

(defun remove-instance (identifier-list)
  "Given one or more identifiers, remove instance from the database if
  present."
  identifier-list)

(defun save-db (filename)
  (with-open-file (out filename
                       :direction :output
                       :if-exists :supersede
		       :if-does-not-exist :create)
    (with-standard-io-syntax
      (print *db* out))))

(defun load-db (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *db* (read in)))))
