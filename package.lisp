(in-package #:cl-user)

(defpackage #:cl-collection-manager-redux
  (:nicknames :collection-manager)
  (:use #:cl
	#:cl-slug
	#:sb-mop))
